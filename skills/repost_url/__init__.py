import logging
from socket import gaierror, getaddrinfo

from opsdroid.events import Message
from opsdroid.skill import Skill
from opsdroid.matchers import match_regex

_LOGGER = logging.getLogger(__name__)

label_re = r'[a-zA-Z0-9][-a-zA-Z-0-9]{0,61}[a-zA-Z0-9]'
domain_re = r'({label}\.)+{label}'.format(label=label_re)


class RepostUrlSkill(Skill):
    @match_regex(domain_re, matching_condition='search')
    async def repost_url(self, message):
        source_rooms = [
            message.connector.room_ids[r] for r
            in self.config.get('source_rooms', [])
        ]

        if message.target not in source_rooms:
            return

        _LOGGER.debug("Matched domain '%s'", message.regex.group(0))

        try:
            getaddrinfo(message.regex.group(0), None)
        except gaierror:
            return

        response = Message(
            '', target=self.config['target_room'],
            connector=message.connector)
        await response.respond(
            '{}: {}'.format(message.user, message.text))
